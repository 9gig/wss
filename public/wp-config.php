<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wsswpdb');

/** MySQL database username */
define('DB_USER', 'wsswpdbu');

/** MySQL database password */
define('DB_PASSWORD', 'qwerty123456Q');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(uo`}Q+k:|}tSW >_ISc[b]@g#?3ZgV$m,OZGUB1}-rGiC!6d1Kyrq59L<$v?i4W');
define('SECURE_AUTH_KEY',  '?8SF@1MHY2}*fi=P7N)ocOG =Mc$*DQtW{+WoGt ~c[a8o_@;<8:ZfH+ie2ep2p+');
define('LOGGED_IN_KEY',    '4a9Wtz-G95 +(aR;;t.Cr:i-aaEvPg .KQ%_Q?MH-n2_g{a[9V{3&^U6cm$@n6||');
define('NONCE_KEY',        'N~]{2AHq1>(xKX%mXv|)=GgWrEx}0 qxja1=`5m,v*t*j8A@s|p =Ue%IT$M[$2C');
define('AUTH_SALT',        '6j3Bf*2-X%r:Oh#@Lxu:eF%kG^@(rii0`}c.|O;=<qOd;c8*<* ;g+8a#pFk(?ZP');
define('SECURE_AUTH_SALT', 'u{@}j(%`m.EF;YJTnQviB;+rWH,+LKTBZ_6;V4QYU%uPz3WI jBCAI^%ne|&EnR!');
define('LOGGED_IN_SALT',   'T}L}eio;a]&8:q8P[D%w5i~?/S|U9R/jW;034+ISVE5bccCqQmAHB53}l{<!Xqw5');
define('NONCE_SALT',       '|AuK{,.NST6vl?>[ffdGhQC&:/xf}ikCCDopdD}n%;=`x8`_1 y*,H`u1:P_]rzJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
