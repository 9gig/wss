<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
            <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php //wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>
		<link href="css/styles.min.css" rel="stylesheet" type="text/css">
		<style type="text/css">
		body, html {
			margin: 0;
			padding: 0;
			font-family: arial, verdana, sans-serif;
		}

		.container {
			display: table;
			margin:auto;
			background-image: url('http://www.website-services.ltd.uk/wp-content/uploads/2016/03/mastbkgrdpaths2.png'); /* thanks to BrokenSphere / Wikimedia Commons for https://commons.wikimedia.org/wiki/File:Stone_Forest_pathway_02.JPG */
			background-repeat: no-repeat;
			background-color: #FFFFFF;
			background-position: center top;	}
		.content { display: table-row; height: 100%; }
		.content-body { display: table-cell; }

		.container { width: 1075px; height: 100%; }
		.header { padding: 17rem 0 0 0; text-align:center;
			border-style: solid;
		    border-bottom-width: 2px;
		    border-top-width: 0px;
		    border-left-width: 0px;
		    border-right-width: 0px;
		    border-color:#aaaaaa;
		}
		.footer { padding: 10px 10px 5px 10px; margin: 0; text-align:center; background-color: #cccccc;
			border-style: solid;
		    border-bottom-width: 10px;
		    border-top-width: 2px;
		    border-left-width: 0px;
		    border-right-width: 0px;
		    border-color:#aaaaaa;
		    border-radius:0 0 1rem 1rem;
		}
		.content-body { height:100%; padding: 15px; background: #e7e7e7; border: 1rem solid #dddddd; }
		.main { display: table; margin:0; height:38rem; width:97%; padding: 13px; background: #888888; border: .2rem solid #777777; }
		.main1 { display: table-row; height: 99%; padding: 3px;background: #888888; border: 0 solid #777777; }
		.main2 { display: table-cell; padding: 12px 45px; height: 98%; background: white; border: .2rem solid #bbbbbb;border-radius:4rem;
		  background: -webkit-linear-gradient(white, #cccccc); /* For Safari 5.1 to 6.0 */
		  background: -o-linear-gradient(white, #cccccc); /* For Opera 11.1 to 12.0 */
		  background: -moz-linear-gradient(white, #cccccc); /* For Firefox 3.6 to 15 */
		  background: linear-gradient(white, #cccccc); /* Standard syntax */
		  background-image: url('http://www.website-services.ltd.uk/wp-content/uploads/2016/03/pathway1367.jpg');
			background-size: 100% 100%;
		    background-attachment: fixed;}
		.main2 p { font-size:1.05rem;text-align:justify;}
		.main3holder {
			display: table-cell;
			float: left;
			margin: 0;
			padding: 0;
			width:550px;
		}
		.main3 {
			display: block;
			float: left;
			margin:40px 20px 0 0;
			padding: 20px;
			width:500px;
			background-color:rgba(255,255,255,0.75);
			border: 3px white solid;
			border-radius: 1rem;
		}
		.main4 {
			display: table-cell;
			float:right;
			width:290px;
			font-size:.8rem;
			margin:55px 0 0 0;
		}
		.main4 p { font-size:.9rem;text-align:justify;}
		.main5 {
			display: block;
			padding: 20px;
			width:840px;
			font-size:1rem;
			margin:40px 0 0 0;
			background-color:rgba(255,255,255,0.75);
			border: 3px white solid;
			border-radius: 1rem;
		}
		.main5 p { font-size:1rem;text-align:justify;}
		a { font-family: arial, verdana, sans-serif; color: #FFECA2; }
		.footer p { padding: 0; margin:.2rem 0 0 0; font-size:.6rem;}

		.header a {
			font-size:1.7rem;
			padding: .1rem 1rem .1rem 1rem;
			margin: .5rem 0 0 0;
			border-radius: .5rem;
			background-color: #444444;
		}
		.header a:hover { color: #ffe271; background-color: #222222; }
		.header .current { color: #ffe271; background-color: #111111;	}
		.main3 a {
			font-size: 1rem;
			padding: .1rem .3rem .1rem .3rem;
			border-radius: .1rem;
			background-color: #777777;
		}
		.main3 a:hover { color: #ffe271;	background-color: #666666; }
		.main5 a {
			font-size: 1rem;
			padding: .1rem .3rem .1rem .3rem;
			border-radius: .1rem;
			background-color: #777777;
		}
		.main5 a:hover { color: #ffe271;	background-color: #666666; }
		.footer a {
			font-size: .8rem;
			padding: .1rem .3rem .1rem .3rem;
			border-radius: .1rem;
			background-color: #777777;
		}
		.footer a:hover { color: #ffe271;	background-color: #666666; }
		.footer .current { color: #ffe271; background-color: #666666;	}
		table {
			border: 2px solid #ffcc00;
			background-color: rgba(255,236,162,0.65);
			border-radius:.5rem;
		}
		td {
			padding:.5rem;
			vertical-align: top;
			font-size:.8rem;
		}
		/* Media Queries */
		@media all and (max-width: 1075px) {
			  .container {
			width: 100%;
			height: 100%;
			background-image: url('http://www.website-services.ltd.uk/wp-content/images/mastbkgrdpaths2.png'), linear-gradient(0deg, #ffffff, #cccccc 80%);
			background-repeat: no-repeat;
			background-position: center top;
			background-size: 100% auto;
			}
			  .main { min-width: 605px;}
			  .main2 {
		 	background-image: url('http://www.website-services.ltd.uk/wp-content/images/pathway1367.jpg');
			background-size: cover;
			background-position: center;
		    background-attachment: fixed;
		    padding: 12px 24px;
		    width: 60%;
			}
			  .main3holder { width: 63%; height: auto;}
			  .main3 { width: 85%; height: auto;}
			  .main4 { width: 35%; height: auto;}
		}
		</style>

		<link rel="stylesheet" type="text/css" href="./css/spn.css" />
		<script src="./js/spn.js" type="text/javascript" async></script>

		<link rel="next" title="Page 1" href="services.html" />

	</head>

<body id="mainb">
	<div class="container">
	    <header class="header">
			<a class="current" href="http://www.website-services.ltd.uk/">Home</a>&nbsp; &nbsp;
			<a href="services/">Services</a>&nbsp; &nbsp;
			<a href="websites/">Websites</a>&nbsp; &nbsp;
			<a href="contact-us/">Contact</a>
	    </header>
	    <section class="content">
	        <div class="content-body">
	            <div class="main">
		            <div class="main1">
			            <div class="main2">
				            <div class="main3holder">
