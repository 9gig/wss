#this is to alias the mysql db directories so we always have the latest copy when we destroy stuff!
#RUN THESE ONLY AFTER THE INITIAL SETUP IS complete
#SECOND RUN
sudo cp /var/www/mysqldata/my.cnf /etc/mysql/my.cnf
sudo cp /var/www/mysqldata/alias /etc/apparmor.d/tunables/alias
sudo service apparmor reload
sudo service mysql restart
#only include the above if the database has already been created!
#/SECOND RUN
sudo useradd -m wsswpuser
#usermod apache --append --groups wsswpuser
#commented out because this is a ubuntu box.....
sudo usermod vagrant --append --groups wsswpuser

chown -R wsswpuser.wsswpuser /var/www/public
chmod -R 0775 /var/www/public

#cat /var/www/ldex1pubkey.txt > .ssh/authorized_keys

#install our website database ONLY RUN THIS THE FIRST TIME YOU'RE INSTALLING FILES
#db="create database wsswpdb;GRANT ALL PRIVILEGES ON wsswpdb.* TO wsswpdbu@localhost IDENTIFIED BY 'qwerty123456Q';FLUSH PRIVILEGES;"
#run this to refresh the privileges
db="GRANT ALL PRIVILEGES ON wsswpdb.* TO wsswpdbu@localhost IDENTIFIED BY 'qwerty123456Q';FLUSH PRIVILEGES;"
mysql -u root -proot -e "$db"
#FIRST RUN
#mysql -u root -proot wsswpdb < /var/www/wsswpdb.sql
#/FIRST RUN

#get the wp-cli script and then use it to install and activate our preferred wordpress theme and plugin/s

wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
mv wp-cli.phar /usr/local/bin/wp
(cd /var/www/public && su wsswpuser -c "wp theme install bones")
(cd /var/www/public && su wsswpuser -c "wp theme install bones-child")
(cd /var/www/public && su wsswpuser -c "wp theme activate bones-child")
#(cd /var/www/html/www.meteormoneymakers.com/httpdocs && su 9mwpuser -c "wp theme install quark")
#(cd /var/www/html/www.meteormoneymakers.com/httpdocs && su 9mwpuser -c "wp theme activate quark")
#(cd /var/www/html/www.meteormoneymakers.com/httpdocs && su 9mwpuser -c "wp plugin install woocommerce")
#(cd /var/www/html/www.meteormoneymakers.com/httpdocs && su 9mwpuser -c "wp plugin activate woocommerce")
#(cd /var/www/html/www.meteormoneymakers.com/httpdocs && su 9mwpuser -c "wp plugin install better-wp-security")
#(cd /var/www/html/www.meteormoneymakers.com/httpdocs && su 9mwpuser -c "wp plugin activate better-wp-security")

#tweak the plugin display as we desire -- first the checkout display removal of registration details
#commented out for pre-existing files boot
#cp ./functions.php /var/www/html/www.meteormoneymakers.com/httpdocs/wp-content/themes/quark/functions.php
#chown 9mwpuser.9mwpuser /var/www/html/www.meteormoneymakers.com/httpdocs/wp-content/themes/quark/functions.php
